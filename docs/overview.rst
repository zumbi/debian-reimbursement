========
Overview
========

This is a Django application that implements the expense reimbursement
workflow for the `Debian <https://www.debian.org/>`_ project.
Project Members who wish to make expenses on behalf on the project file
a request for approval, and then claim their reimbursement through this
application.

Request States
==============

Requests go through this state life-cycle:

.. graphviz::

   digraph {
     "[New]" [style="filled", fillcolor="/brbg3/3"];
     "[New]" -> "Draft" [label="Requester creates"];
     "Draft" -> "Pending Approval" [label="Requester proposes"];
     "Pending Approval" -> "Approved" [label="Approver approves", color="/accent3/1"];
     "Pending Approval" -> "Deleted" [label="Approver rejects", color="/accent3/1"];
     "Pending Approval" -> "Draft" [label="Approver requests more information", color="/accent3/1"];
     "Approved" -> "Draft" [label="Requestor needs budget increase"];
     "Approved" -> "Pending Payment" [label="Requester submits receipts"];
     "Pending Payment" -> "Paid" [label="Payer reimburses requester", color="/accent3/2"];
     "Deleted" [style="filled", fillcolor="/brbg3/1"];
     "Paid" [style="filled", fillcolor="/brbg3/1"];
   }
