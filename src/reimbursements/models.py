from collections import defaultdict
from decimal import Decimal

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from historical_currencies.exceptions import ExchangeRateUnavailable
from historical_currencies.exchange import exchange

from reimbursements.storage import generate_receipt_filename, receipts_storage


User = get_user_model()


class SoftDeletionManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(deleted=False)


class Profile(models.Model):
    bank_details = models.JSONField(blank=True)
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f"Profile for {self.user}"


class RequestType(models.Model):
    name = models.CharField(max_length=50)
    deleted = models.BooleanField(default=False)

    objects = SoftDeletionManager()
    all_objects = models.Manager()

    def __str__(self):
        return self.name

    @classmethod
    def expense_type_map(cls):
        output = defaultdict(list)
        for row in RequestType.expense_types.through.objects.all():
            output[row.requesttype_id].append(row.expensetype_id)
        return output


class ExpenseType(models.Model):
    request_types = models.ManyToManyField(RequestType, related_name="expense_types")
    name = models.CharField(max_length=50, unique=True)
    deleted = models.BooleanField(default=False)

    objects = SoftDeletionManager()
    all_objects = models.Manager()

    def __str__(self):
        return self.name


class ExpenseTypeAllocation:
    def __init__(
        self, name, currency, requested=0, spent=0, reimbursed=0, special=False
    ):
        self.name = name
        self.currency = currency
        self.requested = Decimal(requested)
        self.spent = Decimal(spent)
        self.reimbursed = Decimal(reimbursed)
        self.special = special

    @property
    def qualified_requested(self):
        return (self.requested, self.currency)

    @property
    def qualified_spent(self):
        return (self.spent, self.currency)

    @property
    def qualified_reimbursed(self):
        return (self.reimbursed, self.currency)

    @property
    def remaining(self):
        return self.requested - self.spent

    @property
    def qualified_remaining(self):
        return (self.remaining, self.currency)

    @property
    def over_budget(self):
        return self.spent > self.requested

    @property
    def within_budget(self):
        return not self.over_budget

    @property
    def approved(self):
        if self.within_budget:
            return self.requested
        return self.requested * (
            1 + Decimal(settings.OVERBUDGET_LEEWAY_PERCENTAGE) / 100
        )

    @property
    def reimburseable(self):
        return min(self.approved, self.spent)

    @property
    def qualified_approved(self):
        return (self.approved, self.currency)

    @property
    def qualified_reimburseable(self):
        return (self.reimburseable, self.currency)

    def to_dict(self):
        return {
            "currency": self.currency,
            "requested": float(self.requested),
            "spent": float(self.spent),
            "reimbursed": float(self.reimbursed),
        }


class Request(models.Model):
    class States(models.TextChoices):
        DRAFT = "draft", _("Draft")
        PENDING_APPROVAL = "pending_approval", _("Pending Approval")
        APPROVED = "approved", _("Approved")
        DELETED = "deleted", _("Deleted")
        PENDING_PAYMENT = "pending_payment", _("Pending Payment")
        PAID = "paid", _("Paid")

    requester = models.ForeignKey(User, on_delete=models.PROTECT)
    parent = models.ForeignKey(
        "Request",
        on_delete=models.PROTECT,
        null=True,
        related_name="children",
        blank=True,
    )
    state = models.CharField(
        max_length=16, choices=States.choices, default=States.DRAFT
    )
    bank_details = models.JSONField(null=True, blank=True)
    request_type = models.ForeignKey(RequestType, on_delete=models.PROTECT)
    description = models.TextField()
    currency = models.CharField(max_length=3, default=settings.DEFAULT_CURRENCY)
    approver_group = models.ForeignKey(
        Group, on_delete=models.PROTECT, null=True, related_name="requests_approver"
    )
    payer_group = models.ForeignKey(
        Group, on_delete=models.PROTECT, related_name="requests_payer"
    )

    @property
    def subject(self):
        lines = self.description.splitlines()
        if lines:
            return lines[0].strip()
        return ""

    @property
    def total(self):
        return (
            self.lines.aggregate(total=models.Sum("amount"))["total"] or Decimal(0),
            self.currency,
        )

    @property
    def receipts_total(self):
        lines = ReceiptLine.objects.filter(receipt__request=self)
        return (
            lines.aggregate(total=models.Sum("converted_amount"))["total"]
            or Decimal(0),
            self.currency,
        )

    @property
    def reimbursed_total(self):
        lines = ReimbursementLine.objects.filter(request=self)
        return (
            lines.aggregate(total=models.Sum("amount"))["total"] or Decimal(0),
            self.currency,
        )

    @property
    def created(self):
        return self.history.get(event=RequestHistory.Events.CREATED).timestamp

    @property
    def updated(self):
        return self.history.all().last().timestamp

    def get_absolute_url(self):
        return reverse("request", kwargs={"pk": self.pk})

    def get_fully_qualified_url(self):
        return settings.SITE_URL.rstrip("/") + self.get_absolute_url()

    def summarize_by_type(self):
        summary = {}
        total = ExpenseTypeAllocation("Total", self.currency, special=True)
        for line in self.lines.all():
            if line.expense_type_id not in summary:
                summary[line.expense_type_id] = ExpenseTypeAllocation(
                    line.expense_type.name, self.currency
                )
            summary[line.expense_type_id].requested += line.amount
            total.requested += line.amount
        for line in self.reimbursement_lines.all():
            if line.expense_type_id not in summary:
                summary[line.expense_type_id] = ExpenseTypeAllocation(
                    line.expense_type.name, self.currency
                )
            summary[line.expense_type_id].reimbursed += line.amount
            total.reimbursed += line.amount
        for receipt in self.receipts.all():
            for line in receipt.lines.all():
                if line.expense_type_id not in summary:
                    summary[line.expense_type_id] = ExpenseTypeAllocation(
                        line.expense_type.name, self.currency
                    )
                summary[line.expense_type_id].spent += line.converted_amount
                total.spent += line.converted_amount
        summary["total"] = total
        return summary

    def summarize_by_type_dict(self):
        allocation = {}
        for k, v in self.summarize_by_type().items():
            allocation[v.name] = v.to_dict()
        return allocation

    @property
    def approved_budget_limit(self):
        """The total requested, plus acceptable leeway"""
        total = self.total[0]
        approved_limit = total * (
            1 + Decimal(settings.OVERBUDGET_LEEWAY_PERCENTAGE) / 100
        )
        return (approved_limit, self.currency)

    @property
    def total_reimburseable(self):
        """The total requested, cut off at approved_budget_limit"""
        approved_total = min(self.approved_budget_limit[0], self.receipts_total[0])
        return (approved_total, self.currency)

    @property
    def within_budget(self):
        budget = self.approved_budget_limit[0]
        receipts_total = self.receipts_total[0]
        return receipts_total <= budget

    @property
    def over_budget(self):
        return not self.within_budget

    def __str__(self):
        return f"Request {self.id}: {self.subject}"

    def user_access(self, user):
        """What access levels does user have to this Request"""
        access = {}
        if self.requester == user:
            access["requester"] = True
        if user.groups.filter(id=self.approver_group_id).exists():
            access["approver"] = True
        if user.groups.filter(id=self.payer_group_id).exists():
            access["payer"] = True
        return access

    @property
    def requester_name(self):
        """Helper for templates, to render the requester's name"""
        name = self.requester.get_full_name()
        if not name:
            name = self.requester.username
        return name


class RequestLine(models.Model):
    request = models.ForeignKey(Request, on_delete=models.CASCADE, related_name="lines")
    expense_type = models.ForeignKey(ExpenseType, on_delete=models.PROTECT)
    description = models.TextField()
    amount = models.DecimalField(decimal_places=2, max_digits=15)

    @property
    def qualified_amount(self):
        return (self.amount, self.request.currency)

    def clean(self):
        super().clean()
        request_type_id = self.request.request_type_id
        if (
            request_type_id
            and self.expense_type_id
            and not self.expense_type.request_types.filter(pk=request_type_id)
        ):
            raise ValidationError(
                {
                    "expense_type": _("%s is not a valid expense type for %s requests")
                    % (self.expense_type.name, self.request.request_type.name),
                }
            )


class RequestHistory(models.Model):
    class Events(models.TextChoices):
        CREATED = "created", _("Created")
        UPDATED = "updated", _("Updated")
        PROPOSED = "proposed", _("Proposed")
        APPROVED = "approved", _("Approved")
        REJECTED = "rejected", _("Rejected")
        MOREINFO_APPROVAL = (
            "moreinfo_approval",
            _("Information requested for approval"),
        )
        WITHDRAWN = "withdrawn", _("Withdrawn")
        SUBMITTED = "submitted", _("Submitted")
        REIMBURSED = "reimbursed", _("Reimbursed")
        MOREINFO_REIMBURSAL = (
            "moreinfo_reimbursal",
            _("Information requested for reimbursement"),
        )
        NOTE = "note", _("Note")
        RECEIPT_ADDED = "receipt_added", _("Receipt Added")
        RECEIPT_DELETED = "receipt_deleted", _("Receipt Deleted")
        RECEIPT_UPDATED = "receipt_updated", _("Receipt Updated")
        BUDGET_INCREASE = "budget_increase", _("Returned for budget increase")

    request = models.ForeignKey(
        Request, on_delete=models.CASCADE, related_name="history"
    )
    actor = models.ForeignKey(User, on_delete=models.PROTECT)
    timestamp = models.DateTimeField(auto_now_add=True)
    event = models.CharField(max_length=20, choices=Events.choices)
    budget = models.JSONField(blank=True, null=True)
    details = models.TextField()

    class Meta:
        ordering = ["timestamp"]
        verbose_name = _("Request history event")
        verbose_name_plural = _("Request history")

    def __str__(self):
        return f"{self.timestamp} {self.actor.username} {self.event}"

    def budget_total_qualified(self):
        if self.budget:
            total = self.budget["Total"]
            return (Decimal(total["requested"]), total["currency"])

    def budget_allocation(self):
        for name, line in sorted(self.budget.items()):
            if name == "Total":
                continue
            yield ExpenseTypeAllocation(name=name, **line)
        yield ExpenseTypeAllocation(name=name, **self.budget["Total"], special=True)


class Receipt(models.Model):
    request = models.ForeignKey(
        Request, on_delete=models.CASCADE, related_name="receipts"
    )
    file = models.FileField(
        storage=receipts_storage, upload_to=generate_receipt_filename
    )
    description = models.TextField()

    @property
    def subject(self):
        lines = self.description.splitlines()
        if lines:
            return lines[0].strip()
        return ""

    @property
    def total(self):
        return (
            self.lines.aggregate(total=models.Sum("converted_amount"))["total"]
            or Decimal(0),
            self.request.currency,
        )

    def __str__(self):
        return f"{self.request}: {self.description}"


class ReceiptLine(models.Model):
    receipt = models.ForeignKey(Receipt, on_delete=models.CASCADE, related_name="lines")
    date = models.DateField()
    description = models.TextField()
    amount = models.DecimalField(decimal_places=2, max_digits=15)
    currency = models.CharField(max_length=3)
    converted_amount = models.DecimalField(decimal_places=2, max_digits=15)
    expense_type = models.ForeignKey(ExpenseType, on_delete=models.PROTECT)

    @property
    def qualified_amount(self):
        return (self.amount, self.currency)

    @property
    def qualified_converted_amount(self):
        return (self.converted_amount, self.receipt.request.currency)

    def clean(self):
        super().clean()
        request = None
        currency_to = getattr(self, "initial_request_currency", None)
        try:
            request = self.receipt.request
            currency_to = request.currency
        except Receipt.DoesNotExist:
            pass
        except Request.DoesNotExist:
            pass
        if self.amount and self.currency and currency_to:
            try:
                self.converted_amount = exchange(
                    amount=self.amount,
                    currency_from=self.currency,
                    currency_to=currency_to,
                    date=self.date,
                )
            except ExchangeRateUnavailable:
                raise ValidationError(
                    {
                        "currency": _(
                            "No exchange rate available for %(currency_from)s "
                            "to %(currency_to)s for %(date)s. Please contact "
                            "an admin to get it imported."
                        )
                        % {
                            "currency_from": self.currency,
                            "currency_to": currency_to,
                            "date": self.date,
                        }
                    }
                )

        if (
            request
            and self.expense_type_id
            and not self.expense_type.request_types.filter(pk=request.request_type.pk)
        ):
            raise ValidationError(
                {
                    "expense_type": _("%s is not a valid expense type for %s requests")
                    % (self.expense_type.name, request.request_type.name),
                }
            )


@receiver(post_save, sender=Request)
def update_receipt_conversions(sender, instance, **kwargs):
    """Update converted_amounts, if the request currency changed"""
    for receipt in instance.receipts.all():
        for line in receipt.lines.all():
            line.clean()
            line.save()


class ReimbursementLine(models.Model):
    request = models.ForeignKey(
        Request, on_delete=models.CASCADE, related_name="reimbursement_lines"
    )
    expense_type = models.ForeignKey(ExpenseType, on_delete=models.PROTECT)
    description = models.TextField()  # TODO: Delete?
    amount = models.DecimalField(decimal_places=2, max_digits=15)

    @property
    def qualified_amount(self):
        return (self.amount, self.request.currency)


class ReimbursementCost(models.Model):
    request = models.ForeignKey(
        Request, on_delete=models.CASCADE, related_name="reimbursement_cost"
    )
    description = models.TextField()
    amount = models.DecimalField(decimal_places=2, max_digits=15)
    currency = models.CharField(max_length=3, default=settings.DEFAULT_CURRENCY)

    @property
    def qualified_amount(self):
        return (self.amount, self.request.currency)


class RTTicket(models.Model):
    request = models.ForeignKey(
        Request, on_delete=models.CASCADE, related_name="rt_tickets"
    )
    # We duplicate payer_group here so that tickets get retained even if
    # requests bounce between payers
    payer_group = models.ForeignKey(
        Group,
        on_delete=models.PROTECT,
    )
    ticket_id = models.IntegerField()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["request", "payer_group"], name="One RT Ticket per payer_group"
            ),
            models.UniqueConstraint(
                fields=["payer_group", "ticket_id"], name="One Request per RT Ticket"
            ),
        ]

    @property
    def url(self):
        instance = settings.RT_INSTANCES[self.payer_group.name]
        return f"{instance['url']}/Ticket/Display.html?id={self.ticket_id}"
