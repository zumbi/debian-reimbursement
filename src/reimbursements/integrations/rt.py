import re
import logging
from typing import Any, Optional
from urllib.parse import urlencode

from django.conf import settings
from django.contrib.auth.models import Group
from django.template.loader import render_to_string

import requests

from debian_reimbursements.context_processors import meta
from reimbursements.models import Request, RTTicket

log = logging.getLogger(__name__)


class RTException(Exception):
    """We failed to perform an action on RT"""


class RT:
    url: str
    cc: list[str]
    queue: str
    api_token: Optional[str]
    username: Optional[str]
    password: Optional[str]

    def __init__(self, group: Group):
        instance = settings.RT_INSTANCES[group.name]
        self.url = instance["url"]
        self.cc = instance["cc"]
        self.queue = instance["queue"]
        self.api_token = instance.get("api_token")
        if self.api_token:
            self.username = self.password = None
        else:
            self.username = instance["username"]
            self.password = instance["password"]

    @classmethod
    def has_instance_for_group(cls, group: Group) -> bool:
        return group.name in settings.RT_INSTANCES

    def _post(self, endpoint: str, dict_body: dict[str, str]) -> list[str]:
        body = "\n".join(
            k + ": " + str(v).replace("\n", "\n ") for k, v in dict_body.items()
        )
        data = {"content": body}
        headers = {}
        params = {}
        if self.api_token:
            headers = {"Authorization": f"token {self.api_token}"}
        else:
            params = {
                "user": self.username,
                "pass": self.password,
            }

        url = self.url + "/REST/1.0" + endpoint
        if params:
            url += "?" + urlencode(params)
        r = requests.post(url, data=data, headers=headers)
        if r.status_code != 200:
            raise RTException(f"RT POST failed: {r.status_code}")
        lines = r.text.splitlines()
        if not lines:
            raise RTException("Empty response from RT")
        statusline = lines[0]
        m = re.match(r"^RT/(?P<version>\S+) (?P<code>\d+) (?P<msg>.+)$", statusline)
        if not m:
            raise RTException(f"Could not parse RT status line: {statusline!r}")
        statuscode = int(m.group("code"))
        if statuscode != 200:
            raise RTException(f"RT POST failed (RT Status): {statuscode}")
        blankline = lines[1]
        if blankline != "":
            raise RTException(f"RT: Expected blank line after status, not {blankline}")
        commentline = lines[2]
        if not commentline.startswith("#"):
            raise RTException(
                f"RT: Expected comment line after status, not {commentline}"
            )
        return lines

    def create_ticket(self, request: Request, ticket_body: str) -> int:
        dict_body = {
            "id": "ticket/new",
            "Queue": self.queue,
            "Requestor": request.requester.email,
            "Subject": f"{settings.SITE_NAME} {request}",
            "Cc": ", ".join(self.cc),
            "AdminCc": "",
            "Owner": "Nobody",
            "Status": "new",
            "Priority": 0,
            "InitialPriority": 0,
            "FinalPriority": 0,
            "TimeEstimated": 0,
            "Starts": "",
            "Due": "",
            "Text": ticket_body,
        }
        response = self._post("/ticket/new", dict_body)
        m = re.match(r"^# Ticket (\d+) created\.$", response[2])
        if not m:
            raise RTException(
                f"Could not determine RT Ticket number from response: {response}"
            )
        ticket_id = int(m.group(1))
        RTTicket.objects.create(
            request=request,
            payer_group=request.payer_group,
            ticket_id=ticket_id,
        )
        return ticket_id

    def comment_ticket(self, request: Request, comment_body: str):
        dict_body = {
            "Action": "comment",
            "Text": comment_body,
        }
        ticket_id = RTTicket.objects.get(
            request=request, payer_group=request.payer_group
        ).ticket_id
        self._post(f"/ticket/{ticket_id}/comment", dict_body)

    def close_ticket(self, request: Request):
        dict_body = {
            "Status": "resolved",
        }
        ticket_id = RTTicket.objects.get(
            request=request, payer_group=request.payer_group
        ).ticket_id
        self._post(f"/ticket/{ticket_id}/edit", dict_body)

    @classmethod
    def render_template(
        cls,
        request: Request,
        template_name: str,
        context: Optional[dict[str, Any]] = None,
    ) -> str:
        if context is None:
            context = {}
        context.setdefault("request", request)
        context.update(meta(None))
        return render_to_string(template_name, context)

    @classmethod
    def create_or_update_ticket(
        cls,
        request: Request,
        template_name: str,
        context: Optional[dict[str, Any]] = None,
    ):
        """
        Create/update a ticket

        Render template using context, comment on the ticket, if it exists,
        create it if not.
        """
        rt_instance = cls(request.payer_group)
        rt_tickets = RTTicket.objects.filter(
            request=request, payer_group=request.payer_group
        )
        ticket_body = cls.render_template(request, template_name, context)
        if rt_tickets.exists():
            rt_instance.comment_ticket(request, ticket_body)
        else:
            rt_instance.create_ticket(request, ticket_body)

    @classmethod
    def update_ticket_if_exists(
        cls,
        request: Request,
        template_name: str,
        context: Optional[dict[str, Any]] = None,
        close=False,
    ):
        """
        Update (and optionally close) a ticket if it exists

        Render template using context, comment on the ticket, and optionally
        close it.
        """
        rt_tickets = RTTicket.objects.filter(
            request=request, payer_group=request.payer_group
        )
        if not rt_tickets.exists():
            return

        rt_instance = cls(request.payer_group)
        body = cls.render_template(request, template_name, context)
        rt_instance.comment_ticket(request, body)

        if close:
            try:
                rt_instance.close_ticket(request)
            except RTException as e:
                # TODO: Check status and handle tickets that are already closed
                log.warn("Failed to close RT ticket: %s", e)
