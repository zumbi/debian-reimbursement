from datetime import date, datetime
from decimal import Decimal

from django.test import SimpleTestCase, TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError

from historical_currencies.exchange import exchange
from historical_currencies.models import ExchangeRate

from reimbursements.models import (
    ExpenseType,
    ExpenseTypeAllocation,
    Profile,
    RTTicket,
    Receipt,
    ReceiptLine,
    ReimbursementCost,
    ReimbursementLine,
    Request,
    RequestHistory,
    RequestLine,
    RequestType,
)


class RequestHelpers:
    def create_request(self):
        User = get_user_model()
        self.user = User.objects.create_user(username="user")
        self.approvers = Group.objects.create(name="approvers")
        self.payers = Group.objects.create(name="payers")
        self.travel = RequestType.objects.create(name="Travel")
        self.accomm = ExpenseType.objects.create(name="Accommodation")
        self.accomm.request_types.set([self.travel])
        self.request = Request.objects.create(
            requester=self.user,
            request_type=self.travel,
            description="A Test Request",
            currency="USD",
            approver_group=self.approvers,
            payer_group=self.payers,
        )
        RequestHistory.objects.create(
            request=self.request,
            actor=self.user,
            event=RequestHistory.Events.CREATED,
        )
        return self.request

    def create_request_line(self, expense_type=None):
        if expense_type is None:
            expense_type = self.accomm

        return RequestLine.objects.create(
            request=self.request,
            expense_type=expense_type,
            description="Accommodation",
            amount=Decimal(100),
        )

    def create_receipt(self):
        return Receipt.objects.create(
            request=self.request, file=None, description="A Test Receipt"
        )

    def create_receipt_line(
        self, receipt, amount=Decimal(59), currency="EUR", expense_type=None
    ):
        if expense_type is None:
            expense_type = self.accomm

        ExchangeRate.objects.update_or_create(
            date=date(2023, 1, 21),
            base_currency="USD",
            currency="EUR",
            rate=Decimal("0.91934"),
        )

        converted_amount = exchange(
            amount, currency, self.request.currency, date(2023, 1, 21)
        )

        return ReceiptLine.objects.create(
            receipt=receipt,
            date=date(2023, 1, 21),
            description="Test Hotel",
            amount=amount,
            converted_amount=converted_amount,
            currency=currency,
            expense_type=expense_type,
        )

    def create_reimbursement(self, expense_type=None, amount=Decimal("64.18")):
        if expense_type is None:
            expense_type = self.accomm

        return ReimbursementLine.objects.create(
            request=self.request,
            expense_type=expense_type,
            description="Reimbursed",
            amount=amount,
        )


class TestProfile(SimpleTestCase):
    def test_str(self):
        User = get_user_model()
        profile = Profile(user=User(username="user"), bank_details={})
        self.assertEqual(str(profile), "Profile for user")


class TestRequestTypeSimpleMethods(SimpleTestCase):
    def test_str(self):
        request_type = RequestType(name="Testing")
        self.assertEqual(str(request_type), "Testing")


class TestRequestType(TestCase):
    def test_expense_type_map(self):
        travel = RequestType.objects.create(name="Travel")
        hardware = RequestType.objects.create(name="Hardware")
        accomm = ExpenseType.objects.create(name="Accommodation")
        accomm.request_types.set([travel])
        other = ExpenseType.objects.create(name="Other")
        other.request_types.set([travel, hardware])
        self.assertEqual(
            RequestType.expense_type_map(),
            {
                accomm.id: [travel.id, other.id],
                hardware.id: [other.id],
            },
        )


class TestExpenseType(SimpleTestCase):
    def test_str(self):
        expense_type = ExpenseType(name="Testing")
        self.assertEqual(str(expense_type), "Testing")


class TestExpenseTypeAllocation(SimpleTestCase):
    def setUp(self):
        self.eta = ExpenseTypeAllocation(
            name="Testing",
            currency="USD",
            requested=100,
            spent=75,
            reimbursed=0,
            special=False,
        )

    def test_minimal_constructor(self):
        eta = ExpenseTypeAllocation(
            name="Testing",
            currency="USD",
        )
        self.assertEqual(eta.requested, Decimal(0))
        self.assertEqual(eta.spent, Decimal(0))
        self.assertIs(eta.special, False)

    def test_qualified_requested(self):
        self.assertEqual(self.eta.qualified_requested, (Decimal(100), "USD"))

    def test_qualified_spent(self):
        self.assertEqual(self.eta.qualified_spent, (Decimal(75), "USD"))

    def test_qualified_reimbursed(self):
        self.assertEqual(self.eta.qualified_reimbursed, (Decimal(0), "USD"))

    def test_remaining(self):
        self.assertEqual(self.eta.remaining, Decimal(25))

    def test_qualified_remaining(self):
        self.assertEqual(self.eta.qualified_remaining, (Decimal(25), "USD"))

    def test_over_budget(self):
        self.assertFalse(self.eta.over_budget)

    def test_within_budget(self):
        self.assertTrue(self.eta.within_budget)

    def test_approved(self):
        self.assertTrue(self.eta.approved)

    def test_reimburseable(self):
        self.assertEqual(self.eta.reimburseable, Decimal(75))

    def test_qualified_approved(self):
        self.assertEqual(self.eta.qualified_approved, (Decimal(100), "USD"))

    def test_qualified_reimburseable(self):
        self.assertEqual(self.eta.qualified_reimburseable, (Decimal(75), "USD"))

    def test_to_dict(self):
        self.assertEqual(
            self.eta.to_dict(),
            {
                "currency": "USD",
                "requested": 100.0,
                "spent": 75.0,
                "reimbursed": 0.0,
            },
        )

    def test_spent_over_budget(self):
        self.eta.spent = Decimal(115)
        self.assertEqual(self.eta.remaining, Decimal(-15))
        self.assertTrue(self.eta.over_budget)
        self.assertFalse(self.eta.within_budget)
        with self.settings(OVERBUDGET_LEEWAY_PERCENTAGE=5):
            self.assertEqual(self.eta.approved, Decimal(105))


class TestRequestSimpleMethods(SimpleTestCase):
    def test_subject_multiline(self):
        request = Request(description="Line 1\nLine 2\nLine 3")
        self.assertEqual(request.subject, "Line 1")

    def test_subject_oneline(self):
        request = Request(description="Line 1")
        self.assertEqual(request.subject, "Line 1")

    def test_subject_empty(self):
        request = Request(description="")
        self.assertEqual(request.subject, "")

    def test_str(self):
        request = Request(id=42, description="Line 1\nLine 2")
        self.assertEqual(str(request), "Request 42: Line 1")


class TestEmptyRequest(TestCase, RequestHelpers):
    def setUp(self):
        self.create_request()

    def test_total(self):
        self.assertEqual(self.request.total, (Decimal(0), "USD"))

    def test_receipts_total(self):
        self.assertEqual(self.request.receipts_total, (Decimal(0), "USD"))

    def test_reimbursed_total(self):
        self.assertEqual(self.request.reimbursed_total, (Decimal(0), "USD"))

    def test_approved_budget_limit(self):
        self.assertEqual(self.request.approved_budget_limit, (Decimal(0), "USD"))

    def test_total_reimburseable(self):
        self.assertEqual(self.request.total_reimburseable, (Decimal(0), "USD"))

    def test_within_budget(self):
        self.assertEqual(self.request.within_budget, True)

    def test_over_budget(self):
        self.assertEqual(self.request.over_budget, False)

    def test_created(self):
        self.assertIsInstance(self.request.created, datetime)

    def test_updated(self):
        self.assertEqual(self.request.created, self.request.updated)

    def test_absolute_url(self):
        self.assertEqual(
            self.request.get_absolute_url(), f"/requests/{self.request.id}"
        )

    def test_fully_qualified_url(self):
        self.assertEqual(
            self.request.get_fully_qualified_url(),
            f"https://reimbursements.example.com/requests/{self.request.id}",
        )

    def test_summarize_by_type(self):
        summary = self.request.summarize_by_type()
        self.assertIsInstance(summary, dict)
        self.assertEqual(list(summary.keys()), ["total"])
        total = summary["total"]
        self.assertTrue(total.special)
        self.assertEqual(total.name, "Total")
        self.assertEqual(total.currency, "USD")
        self.assertEqual(total.spent, Decimal(0))
        self.assertEqual(total.reimbursed, Decimal(0))

    def test_summarize_by_type_dict(self):
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Total": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 0.0,
                    "spent": 0.0,
                },
            },
        )

    def test_requester_name_named(self):
        self.user.first_name = "Fake"
        self.user.last_name = "User"
        self.user.save()
        self.assertEqual(self.request.requester_name, "Fake User")

    def test_requester_name_fallback(self):
        self.assertEqual(self.request.requester_name, "user")

    def test_user_access_requester(self):
        self.assertEqual(self.request.user_access(self.user), {"requester": True})

    def test_user_access_approver(self):
        User = get_user_model()
        user2 = User.objects.create_user(username="user2")
        user2.groups.set([self.approvers])
        self.assertEqual(self.request.user_access(user2), {"approver": True})

    def test_user_access_payer(self):
        User = get_user_model()
        user2 = User.objects.create_user(username="user2")
        user2.groups.set([self.payers])
        self.assertEqual(self.request.user_access(user2), {"payer": True})

    def test_user_access_unrelated(self):
        User = get_user_model()
        user2 = User.objects.create_user(username="user2")
        self.assertEqual(self.request.user_access(user2), {})


class TestNonTrivialRequests(TestCase, RequestHelpers):
    def test_request_lines(self):
        self.create_request()
        self.create_request_line()
        self.assertEqual(self.request.total, (Decimal(100), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 0.0,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 0.0,
                },
            },
        )
        with self.settings(OVERBUDGET_LEEWAY_PERCENTAGE=5):
            self.assertEqual(self.request.approved_budget_limit, (Decimal(105), "USD"))

    def test_receipt_lines(self):
        self.create_request()
        self.create_request_line()
        self.create_receipt_line(self.create_receipt())
        self.assertEqual(self.request.receipts_total, (Decimal("64.18"), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 64.18,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 64.18,
                },
            },
        )
        with self.settings(OVERBUDGET_LEEWAY_PERCENTAGE=5):
            self.assertEqual(
                self.request.total_reimburseable, (Decimal("64.18"), "USD")
            )
            self.assertTrue(self.request.within_budget)

    def test_receipt_lines_over_budget(self):
        self.create_request()
        self.create_request_line()
        receipt = self.create_receipt()
        self.create_receipt_line(receipt)
        self.create_receipt_line(receipt)
        self.assertEqual(self.request.receipts_total, (Decimal("128.36"), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 128.36,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 128.36,
                },
            },
        )
        with self.settings(OVERBUDGET_LEEWAY_PERCENTAGE=5):
            self.assertEqual(self.request.total_reimburseable, (Decimal(105), "USD"))
            self.assertTrue(self.request.over_budget)

    def test_receipt_lines_across_types(self):
        self.create_request()
        self.create_request_line()
        receipt = self.create_receipt()
        self.create_receipt_line(receipt)
        trains = ExpenseType.objects.create(name="Train Tickets")
        self.create_receipt_line(receipt, expense_type=trains)
        self.assertEqual(self.request.receipts_total, (Decimal("128.36"), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 64.18,
                },
                "Train Tickets": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 0.0,
                    "spent": 64.18,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 128.36,
                },
            },
        )
        with self.settings(OVERBUDGET_LEEWAY_PERCENTAGE=5):
            self.assertEqual(self.request.total_reimburseable, (Decimal(105), "USD"))
            self.assertTrue(self.request.over_budget)

    def test_reimbursement_lines(self):
        self.create_request()
        self.create_request_line()
        self.create_receipt_line(self.create_receipt())
        self.create_reimbursement(amount=Decimal(13))
        self.assertEqual(self.request.reimbursed_total, (Decimal(13), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 13.0,
                    "requested": 100.0,
                    "spent": 64.18,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 13.0,
                    "requested": 100.0,
                    "spent": 64.18,
                },
            },
        )

    def test_reimbursement_lines_across_types(self):
        self.create_request()
        self.create_request_line()
        receipt = self.create_receipt()
        self.create_receipt_line(receipt)
        trains = ExpenseType.objects.create(name="Train Tickets")
        self.create_receipt_line(receipt, expense_type=trains, amount=Decimal(10))
        self.create_reimbursement()
        self.create_reimbursement(expense_type=trains, amount=Decimal("10.88"))
        self.assertEqual(self.request.reimbursed_total, (Decimal("75.06"), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 64.18,
                    "requested": 100.0,
                    "spent": 64.18,
                },
                "Train Tickets": {
                    "currency": "USD",
                    "reimbursed": 10.88,
                    "requested": 0.0,
                    "spent": 10.88,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 75.06,
                    "requested": 100.0,
                    "spent": 75.06,
                },
            },
        )


class TestRequestLineSimpleMethods(SimpleTestCase):
    def test_qualified_amount(self):
        line = RequestLine(request=Request(currency="EUR"), amount=Decimal(100))
        self.assertEqual(line.qualified_amount, (Decimal(100), "EUR"))


class TestRequestLine(TestCase, RequestHelpers):
    def test_clean(self):
        self.create_request()
        line = self.create_request_line()

        self.assertIsNone(line.clean())

    def test_clean_rejects_invalid_expense_type(self):
        self.create_request()
        unrelated = ExpenseType.objects.create(name="Unrelated")
        line = self.create_request_line(expense_type=unrelated)

        with self.assertRaises(ValidationError) as cm:
            line.clean()

        self.assertIn("expense_type", cm.exception.error_dict)
        self.assertEqual(len(cm.exception.error_dict["expense_type"]), 1)
        inner_error = cm.exception.error_dict["expense_type"][0]
        self.assertEqual(
            inner_error.messages,
            ["Unrelated is not a valid expense type for Travel requests"],
        )


class TestRequestHistory(SimpleTestCase):
    def setUp(self):
        self.budget = {
            "Category A": {
                "requested": 12,
                "currency": "USD",
            },
            "Category B": {
                "requested": 25,
                "currency": "USD",
            },
            "Total": {
                "requested": 37,
                "currency": "USD",
            },
        }

    def test_str(self):
        User = get_user_model()
        line = RequestHistory(
            actor=User(username="actor"),
            timestamp=datetime(2023, 1, 21, 13, 33, 47),
            event=RequestHistory.Events.CREATED,
        )
        self.assertEqual(str(line), "2023-01-21 13:33:47 actor created")

    def test_budget_total_qualified(self):
        line = RequestHistory(budget=self.budget)
        self.assertEqual(line.budget_total_qualified(), (Decimal(37), "USD"))

    def test_budget_total_qualified_unspecified(self):
        line = RequestHistory()
        self.assertIsNone(line.budget_total_qualified())

    def test_budget_allocation(self):
        line = RequestHistory(budget=self.budget)

        allocation = list(line.budget_allocation())

        self.assertEqual(len(allocation), 3)
        self.assertIsInstance(allocation[0], ExpenseTypeAllocation)

        self.assertEqual(allocation[0].name, "Category A")
        self.assertEqual(allocation[0].requested, Decimal(12))
        self.assertEqual(allocation[0].currency, "USD")

        self.assertEqual(allocation[1].name, "Category B")
        self.assertEqual(allocation[1].requested, Decimal(25))
        self.assertEqual(allocation[1].currency, "USD")

        self.assertEqual(allocation[2].name, "Total")
        self.assertEqual(allocation[2].requested, Decimal(37))
        self.assertEqual(allocation[2].currency, "USD")


class TestReceiptSimpleMethods(SimpleTestCase):
    def test_subject(self):
        receipt = Receipt(description="Line 1\nLine 2\nLine 3")
        self.assertEqual(receipt.subject, "Line 1")

    def test_subject_strips(self):
        receipt = Receipt(description=" Line 1\t\nLine 2\nLine 3")
        self.assertEqual(receipt.subject, "Line 1")

    def test_subject_empty(self):
        receipt = Receipt(description="")
        self.assertEqual(receipt.subject, "")

    def test_str(self):
        receipt = Receipt(
            request=Request(id=2, description="I Need Stuff"),
            description="Receipt for stuff",
        )
        self.assertEqual(str(receipt), "Request 2: I Need Stuff: Receipt for stuff")


class TestReceipt(TestCase, RequestHelpers):
    def test_total(self):
        self.create_request()
        receipt = self.create_receipt()
        self.assertEqual(receipt.total, (Decimal(0), "USD"))
        self.create_receipt_line(receipt)
        self.assertEqual(receipt.total, (Decimal("64.18"), "USD"))
        self.create_receipt_line(receipt)
        self.assertEqual(receipt.total, (Decimal("128.36"), "USD"))


class TestReceiptLineSimpleMethods(SimpleTestCase):
    def test_qualified_amount(self):
        line = ReceiptLine(amount=Decimal(12), currency="USD")
        self.assertEqual(line.qualified_amount, (Decimal(12), "USD"))

    def test_qualified_converted_amount(self):
        line = ReceiptLine(
            receipt=Receipt(request=Request(currency="USD")),
            converted_amount=Decimal(13),
            currency="GBP",
        )
        self.assertEqual(line.qualified_converted_amount, (Decimal(13), "USD"))

    def test_clean_without_request(self):
        line = ReceiptLine(
            receipt=Receipt(),
            amount=Decimal(12),
            currency="GBP",
        )
        line.clean()

    def test_clean_without_receipt(self):
        line = ReceiptLine(
            amount=Decimal(12),
            currency="GBP",
        )
        line.clean()


class TestReceiptLine(TestCase, RequestHelpers):
    def test_clean_converts_amount(self):
        self.create_request()
        line = self.create_receipt_line(self.create_receipt())
        line.converted_amount = Decimal(0)
        line.clean()
        self.assertEqual(line.converted_amount, Decimal("64.18"))

    def test_clean_reports_validation_error_without_rates(self):
        self.create_request()
        line = self.create_receipt_line(self.create_receipt())

        line.currency = "GBP"
        with self.assertRaises(ValidationError) as cm:
            line.clean()

        self.assertIn("currency", cm.exception.error_dict)
        self.assertEqual(len(cm.exception.error_dict["currency"]), 1)
        inner_error = cm.exception.error_dict["currency"][0]
        self.assertEqual(
            inner_error.messages,
            [
                "No exchange rate available for GBP to USD for 2023-01-21. "
                "Please contact an admin to get it imported."
            ],
        )

    def test_clean_rejects_invalid_expense_type(self):
        self.create_request()
        unrelated = ExpenseType.objects.create(name="Unrelated")
        receipt = self.create_receipt()
        line = self.create_receipt_line(receipt, expense_type=unrelated)

        with self.assertRaises(ValidationError) as cm:
            line.clean()

        self.assertIn("expense_type", cm.exception.error_dict)
        self.assertEqual(len(cm.exception.error_dict["expense_type"]), 1)
        inner_error = cm.exception.error_dict["expense_type"][0]
        self.assertEqual(
            inner_error.messages,
            ["Unrelated is not a valid expense type for Travel requests"],
        )


class TestUpdateReceiptConversions(TestCase, RequestHelpers):
    def test_updates_conversions(self):
        self.create_request()
        receipt = self.create_receipt()
        line = self.create_receipt_line(receipt)
        # Set in create_receipt_line()
        self.assertEqual(line.converted_amount, Decimal("64.18"))

        # Change request currency
        ExchangeRate.objects.create(
            date=date(2023, 1, 21),
            base_currency="USD",
            currency="GBP",
            rate=Decimal("0.80717"),
        )
        self.request.currency = "GBP"
        self.request.save()
        line = receipt.lines.first()
        self.assertEqual(line.converted_amount, Decimal("51.80"))


class TestReimbursementLine(SimpleTestCase):
    def test_qualified_amount(self):
        line = ReimbursementLine(
            request=Request(currency="EUR"),
            amount=Decimal("19.95"),
        )
        self.assertEqual(line.qualified_amount, (Decimal("19.95"), "EUR"))


class TestReimbursementCost(SimpleTestCase):
    def test_qualified_amount(self):
        cost = ReimbursementCost(request=Request(), amount=Decimal(12), currency="USD")
        self.assertEqual(cost.qualified_amount, (Decimal(12), "USD"))


class TestRTTicket(SimpleTestCase):
    def test_url(self):
        ticket = RTTicket(payer_group=Group(name="Payers"), ticket_id=42)
        with self.settings(RT_INSTANCES={"Payers": {"url": "https://rt.example.com"}}):
            self.assertEqual(
                ticket.url, "https://rt.example.com/Ticket/Display.html?id=42"
            )
