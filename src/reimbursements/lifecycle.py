from functools import wraps

from django.db import transaction

from reimbursements.models import Request, RequestHistory
from reimbursements.email import send_email
from reimbursements.integrations.rt import RT


def state_change(old_state, new_state):
    def decorator(function):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            with transaction.atomic():
                assert request.state == old_state
                request.state = new_state
                request.save()
                return function(request, *args, **kwargs)

        return wrapper

    return decorator


def request_created(request, actor):
    """A new request has been created by actor"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.CREATED,
    ).save()


def request_commented(request, actor, details):
    """An actor leaves a comment on a request"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.NOTE,
        details=details,
    ).save()


@state_change(Request.States.DRAFT, Request.States.PENDING_APPROVAL)
def request_proposed(request, actor, details):
    """An existing request has been submitted for review by actor"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.PROPOSED,
        budget=request.summarize_by_type_dict(),
        details=details,
    ).save()
    send_email(
        "reimbursements/email/request_ack.txt",
        {"object": request},
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_received.txt",
        {"object": request},
        request.approver_group.user_set.all(),
    )


@state_change(Request.States.APPROVED, Request.States.DRAFT)
def request_proposed_increase(request, actor, details):
    """Return an existing approved request to draft status, for a budget increase"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.BUDGET_INCREASE,
        details=details,
    ).save()


@state_change(Request.States.APPROVED, Request.States.PENDING_PAYMENT)
def request_submitted(request, actor):
    """An request has been submitted for reimbursement by actor"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.SUBMITTED,
        budget=request.summarize_by_type_dict(),
    ).save()
    send_email(
        "reimbursements/email/request_submitted.txt",
        {"object": request},
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_pending_payment.txt",
        {"object": request},
        request.payer_group.user_set.all(),
    )
    if RT.has_instance_for_group(request.payer_group):
        RT.create_or_update_ticket(request, "reimbursements/rt/ticket_body.txt")


@state_change(Request.States.PENDING_PAYMENT, Request.States.PAID)
def request_reimbursed(request, actor, details):
    """An request has been reimbursed by actor"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.REIMBURSED,
        details=details,
    ).save()
    send_email(
        "reimbursements/email/request_reimbursed.txt",
        {"object": request},
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_reimbursement_recorded.txt",
        {
            "actor": actor,
            "object": request,
        },
        request.payer_group.user_set.all(),
    )
    RT.update_ticket_if_exists(
        request,
        "reimbursements/rt/reimbursed_body.txt",
        {"details": details},
        close=True,
    )


def request_updated(request, actor):
    """An existing request has been updated by actor"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.UPDATED,
        budget=request.summarize_by_type_dict(),
    ).save()


@state_change(Request.States.PENDING_APPROVAL, Request.States.APPROVED)
def request_approved(request, actor, details):
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.APPROVED,
        budget=request.summarize_by_type_dict(),
        details=details,
    ).save()
    send_email(
        "reimbursements/email/request_approved.txt",
        {
            "details": details,
            "object": request,
        },
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_approved_ack.txt",
        {
            "actor": actor,
            "details": details,
            "object": request,
        },
        request.approver_group.user_set.all(),
    )


@state_change(Request.States.PENDING_APPROVAL, Request.States.DELETED)
def request_rejected(request, actor, details):
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.REJECTED,
        budget=request.summarize_by_type_dict(),
        details=details,
    ).save()
    send_email(
        "reimbursements/email/request_rejected.txt",
        {
            "details": details,
            "object": request,
        },
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_rejected_ack.txt",
        {
            "actor": actor,
            "details": details,
            "object": request,
        },
        request.approver_group.user_set.all(),
    )


@state_change(Request.States.PENDING_APPROVAL, Request.States.DRAFT)
def request_returned_for_more_info(request, actor, details):
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.MOREINFO_APPROVAL,
        details=details,
    ).save()
    send_email(
        "reimbursements/email/request_moreinfo.txt",
        {
            "details": details,
            "object": request,
        },
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_moreinfo_ack.txt",
        {
            "actor": actor,
            "details": details,
            "object": request,
        },
        request.approver_group.user_set.all(),
    )


@state_change(Request.States.PENDING_PAYMENT, Request.States.APPROVED)
def reimbursement_returned_for_more_info(request, actor, details):
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.MOREINFO_REIMBURSAL,
        details=details,
    ).save()
    send_email(
        "reimbursements/email/request_reimbursement_moreinfo.txt",
        {
            "details": details,
            "object": request,
        },
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_reimbursement_moreinfo_ack.txt",
        {
            "actor": actor,
            "details": details,
            "object": request,
        },
        request.payer_group.user_set.all(),
    )
    RT.update_ticket_if_exists(
        request,
        "reimbursements/rt/reimbursement_moreinfo_body.txt",
        {"details": details},
    )


def receipt_created(receipt, actor):
    RequestHistory(
        request=receipt.request,
        actor=actor,
        event=RequestHistory.Events.RECEIPT_ADDED,
    ).save()


def receipt_updated(receipt, actor):
    RequestHistory(
        request=receipt.request,
        actor=actor,
        event=RequestHistory.Events.RECEIPT_UPDATED,
    ).save()


def receipt_deleted(receipt, actor):
    RequestHistory(
        request=receipt.request,
        actor=actor,
        event=RequestHistory.Events.RECEIPT_DELETED,
    ).save()
