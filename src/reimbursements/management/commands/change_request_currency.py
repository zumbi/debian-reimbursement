from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand

from historical_currencies.exchange import exchange
from reimbursements.models import Request


def group_name(arg: str) -> Group:
    """Look up a Group by name"""
    try:
        return Group.objects.get_by_natural_key(arg)
    except Group.DoesNotExist as e:
        raise ValueError(e)


class Command(BaseCommand):
    help = "Change the currency of a request"

    def add_arguments(self, parser):
        parser.add_argument(
            "--request",
            metavar="ID",
            type=int,
            required=True,
            help="Request to operate on",
        )
        parser.add_argument(
            "--currency",
            metavar="CODE",
            type=str,
            required=True,
            help="New currency",
        )
        parser.add_argument(
            "--payer",
            metavar="NAME",
            type=group_name,
            help="New Payer Group",
        )

    def handle(self, *args, **options):
        request = Request.objects.get(id=options["request"])
        assert request.lines.count() == 1
        line = request.lines.first()

        old_currency = request.currency
        old_amount = line.amount
        new_currency = options["currency"]
        new_amount = exchange(
            old_amount, old_currency, new_currency, request.created.date()
        )
        print(f"Converting {old_amount} {old_currency} to {new_amount} {new_currency}")
        line.amount = new_amount
        line.save()
        request.currency = new_currency
        if options["payer"]:
            request.payer_group = options["payer"]
        request.save()
