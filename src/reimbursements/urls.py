from django.urls import path

from reimbursements.views import (
    BankDetailsView,
    ReceiptCreateView,
    ReceiptDeleteView,
    ReceiptEditView,
    ReceiptFileView,
    RequestCommentView,
    RequestCreateView,
    RequestEditView,
    RequestReceiptsView,
    RequestReimburseView,
    RequestReimburseReturnView,
    RequestReviewView,
    RequestSubmitDraftView,
    RequestSubmitIncreaseView,
    RequestSubmitView,
    RequestView,
)

urlpatterns = [
    path("new", RequestCreateView.as_view(), name="request-new"),
    path("<int:pk>", RequestView.as_view(), name="request"),
    path("<int:pk>/edit", RequestEditView.as_view(), name="request-edit"),
    path(
        "<int:pk>/submit-draft",
        RequestSubmitDraftView.as_view(),
        name="request-submit-draft",
    ),
    path("<int:pk>/review", RequestReviewView.as_view(), name="request-review"),
    path("<int:pk>/bank-details", BankDetailsView.as_view(), name="bank-details"),
    path("<int:pk>/submit", RequestSubmitView.as_view(), name="request-submit"),
    path(
        "<int:pk>/reimburse", RequestReimburseView.as_view(), name="request-reimburse"
    ),
    path(
        "<int:pk>/reimburse-return",
        RequestReimburseReturnView.as_view(),
        name="request-reimburse-return",
    ),
    path(
        "<int:pk>/request-increase",
        RequestSubmitIncreaseView.as_view(),
        name="request-submit-increase",
    ),
    path(
        "<int:pk>/request-comment",
        RequestCommentView.as_view(),
        name="request-comment",
    ),
    path("<int:pk>/receipts", RequestReceiptsView.as_view(), name="request-receipts"),
    path(
        "<int:request_id>/receipts/new", ReceiptCreateView.as_view(), name="add-receipt"
    ),
    path(
        "<int:request_id>/receipts/<int:pk>/delete",
        ReceiptDeleteView.as_view(),
        name="delete-receipt",
    ),
    path(
        "<int:request_id>/receipts/<int:pk>/edit",
        ReceiptEditView.as_view(),
        name="edit-receipt",
    ),
    path(
        "<int:request_id>/receipts/<int:pk>/view-file",
        ReceiptFileView.as_view(),
        name="view-receipt-file",
    ),
]
