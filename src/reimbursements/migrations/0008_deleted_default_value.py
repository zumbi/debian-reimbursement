# Generated by Django 4.1.7 on 2023-10-12 07:48

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("reimbursements", "0007_reimbursementcost"),
    ]

    operations = [
        migrations.AlterField(
            model_name="expensetype",
            name="deleted",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="requesttype",
            name="deleted",
            field=models.BooleanField(default=False),
        ),
    ]
