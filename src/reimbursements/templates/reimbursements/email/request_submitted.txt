{% load i18n %}
{% load currency_format %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request {{ subject }} submitted for reimbursement
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with subject=object.subject requester_name=object.requester_name total=object.receipts_total|currency payer_group=object.payer_group.name %}
Dear {{ requester_name }},

Your request for reimbursement for {{ total }} for {{ subject }}
has been received and will be processed by {{ payer_group }}.

You will be notified by email, when this has been completed.

Thank you,

The {{ SITE_NAME }} system on behalf of {{ payer_group }}.
{% endblocktrans %}
{% endblock %}
