{% load i18n %}
{% load currency_format %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request {{ subject }} requires more information
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with subject=object.subject requester_name=object.requester_name total=object.total|currency request_url=object.get_fully_qualified_url approver_group=object.approver_group.name %}
Dear {{ requester_name }},

Before your request for {{ total }} for {{ subject }}
can be approved, some more information is required:

{{ details }}

Please update the request to provide the information requested, and resubmit it
for review:
{{ request_url }}

Thank you,

The {{ SITE_NAME }} system on behalf of {{ approver_group }}.
{% endblocktrans %}
{% endblock %}
