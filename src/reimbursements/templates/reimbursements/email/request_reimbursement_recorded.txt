{% load i18n %}
{% load currency_format %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request reimbursed: {{ subject }}
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with payer_group=object.payer_group.name requester_name=object.requester_name actor_name=actor.get_full_name|default:actor.username request_url=object.get_fully_qualified_url subject=object.subject reimbursed_total=object.reimbursed_total|currency %}
Dear {{ payer_group }} member,

The request for {{ subject }}
by {{ requester_name }}
has been marked as paid by {{ actor_name }}.
The amount reimbursed is {{ reimbursed_total }}.
You can review the full request here:
{{ request_url }}
{% endblocktrans %}
{% endblock %}
