from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView

from reimbursements.models import Request


class IndexView(TemplateView):
    template_name = "index.html"


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = "profile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        reviewable = {}
        payable = {}
        for group in self.request.user.groups.all():
            requests = group.requests_approver.filter(
                state=Request.States.PENDING_APPROVAL
            )
            if requests.exists():
                reviewable[group.name] = requests.all()
            requests = group.requests_payer.filter(state=Request.States.PENDING_PAYMENT)
            if requests.exists():
                payable[group.name] = requests.all()
        context["reviewable"] = reviewable
        context["payable"] = payable
        return context
