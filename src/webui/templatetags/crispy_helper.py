from django import template
from django.utils.module_loading import import_string

register = template.Library()


@register.simple_tag(takes_context=True)
def reimbursement_form_helper(context, helper_name):
    """
    Find the specified Crispy FormHelper and instantiate it.
    Handy when you are crispyifying other apps' forms.
    """
    class_ = import_string(helper_name)
    return class_(request=context.request)
